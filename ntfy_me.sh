#!/bin/bash
#  Check if config directory exists
if [ ! -d "$HOME/.config" ]; then
	mkdir "$HOME/.config"
fi

# Set configuration file variable
CONFIG_FILE="$HOME/.config/.ntfy_me.conf"

#  Function to start the setup wizard if the configuration file does not exist
setup_wizard() {
	#  Check if the configuration file doesn't exists
	if [ ! -f "$CONFIG_FILE" ]; then
		#  Alert the user about the setup wizard
		echo "It appears this is your first time running this program. Welcome to the configuration setup wizard! \n"
		echo "Please enter the Fully Qualified Domain Name (FQDN) of your ntfy.sh server(s) (inside quotation marks, separated by spaces, i.e "server1" "server2"):"
		read URLS
		echo "URLS=\"$URLS\"" >> $CONFIG_FILE
		echo "Please enter the IP address(es) of your ntfy.sh server(s) (same syntax as when adding the URLs):"
	      	read IPS
	       	echo "IPS=\"$IPS\"" >> $CONFIG_FILE
	       
	       	#  Verify configuration creation worked
	       	if [ ! -f "$CONFIG_FILE" ]; then
		       	echo "Sorry, something went wrong when creating the configuration file.. Please try again."
			#  Exit function and throw error
		       	exit 1
	       else
		       echo "Configuration file has successfully been created at $HOME/.config/.ntfy_me.conf"
		       #  Exit function and throw success code
		       exit 0
	       fi
       else
	       #  Read the configuration file
	       source $CONFIG_FILE
	fi
}

#  Start the setup wizard
setup_wizard


#  Check the current user ID to see if user is root
if [ "$CURRENT_USER_ID" == "0" ]; then
	PRIORITY="urgent"
	IS_ROOT="yes"
else
        PRIORITY="high"
        IS_ROOT="no"
fi

#  Curl function for root user
send_root_alert() {
	curl -d "Root login detected on $(hostname), please verify." \
		-H "tag: root-login" \
		-H "priority: $PRIORITY" \
		-s \
		-w '%{http_code}' \
		-o /dev/null "$1"
}

#  Curl function for regular users
send_regular_alert() {
	curl -d "Login login detected on $(hostname) as $(whoami), please verify." \
		-H "tag: login-attempt" \
		-H "priority: $PRIORITY" \
		-s \
		-w '%{http_code}' \
		-o /dev/null "$1"
}

#  Function to send alerts
send_alert() {
	if [ "$IS_ROOT" == "yes" ]; then
		send_root_alert $1
	else
		send_regular_alert $1
	fi
}

# Function to check if curl was successful
is_success() {
  [ "$1" -eq "200" ]
}

# Try curling the URLs
for url in "${URLS[@]}"; do
  response_code=$(send_alert "$url")

  if is_success "$response_code"; then
    echo "Successfully sent sysadmin alert"
    exit 0
  fi
done

# If all URLs fail, try curling the IPs
for ip in "${IPS[@]}"; do
  response_code=$(send_alert "$ip")

  if is_success "$response_code"; then
    echo "Successfully sent sysadmin alert directly using IP"
    exit 0
  fi
done

echo "Failed to curl both URLs and IP addresses."
