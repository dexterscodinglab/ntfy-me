# Ntfy Me!

Ntfy Me! is a simple wrapper for sending alerts about logins on servers using the notification server Ntfy.sh, provided by [ntfy.sh](https://ntfy.sh/), which in turn can be self-hosted very easily using various methods provided by the comprehensive documentation available on their website.

Ntfy Me! simplifies the use of Ntfy.sh if you have multiple self-hosted Ntfy servers, and was created to help me manage my servers easier.

It can be used without having a self-hosted Ntfy server, but it is meant to be used with a self-hosted one as you preferably need to know the IP address of the notification server (and it should thus not change).

## Usage

When you run `./ntfy_me.sh` for the first time, it will start the Setup Wizard to help you configure the program. You can easily use Ntfy Me together with as many notification servers as you like, and Ntfy Me will try each one until it finds a working server to send the notification.

If DNS fails, Ntfy Me will proceed to attempt to send the notification using the notification servers' IP directly. 
